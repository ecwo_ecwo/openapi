package com.ecwo.rest;

import com.ecwo.rest.model.PairsTime;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.GenericType;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Date: 01.02.16
 * Time: 15:43
 */
public class CommonRestServiceTest {
    protected static String srvPath = "http://192.168.56.1:8080/openAPI/rest";

    @Test
    public void testGetPairs() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/common/pairs.xml");
        ClientResponse<List<PairsTime>> clientResponse = clientRequest.get(new GenericType<List<PairsTime>>() {
        });
        Assert.assertTrue(clientResponse.getStatus() == 200);
        List<PairsTime> cStud = clientResponse.getEntity();
        Assert.assertTrue(!cStud.isEmpty());
        for (PairsTime time : cStud) {
            System.out.printf("%d) %s %s-%s %s%n", time.getNum(), time.getStart(), time.getBreakStart(), time.getBreakEnd(), time.getEnd());
        }
        Assert.assertTrue(cStud.size() == 9);


    }
    @Test
    public void testAcademicYear() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/common/ayear.xml");
        ClientResponse<Integer> clientResponse = clientRequest.get(Integer.class);
        Assert.assertTrue(clientResponse.getStatus() == 200);
        Integer year = clientResponse.getEntity();
        Assert.assertNotNull(year);

    }
}
