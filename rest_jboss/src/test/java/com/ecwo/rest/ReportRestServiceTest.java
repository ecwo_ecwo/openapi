package com.ecwo.rest;

import com.ecwo.rest.model.Student;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.GenericType;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * User: tor
 * Date: 31.07.14
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
public class ReportRestServiceTest {
    private static String srvPath = "http://192.168.56.1:8080/openAPI/rest";

    @Test
    @Ignore
    public void testPrintVersion() throws Exception {

    }

    @Test
    public void testViewRaw() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/report/students/1.json");
        ClientResponse<String> clientResponse = clientRequest.get(String.class);
        if (clientResponse.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Failed : HTTP error code : " + clientResponse.getStatus());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(clientResponse.getEntity().getBytes())));

        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }

    }

    @Test
    public void testGetStudentById_json() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/report/students/1.json");
        ClientResponse<Student> clientResponse = clientRequest.get(Student.class);
        Student cStud = clientResponse.getEntity();
        Assert.assertTrue(cStud.getId() == 1);
    }

    @Test
    public void testGetStudentById_xml() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/report/students/1.xml");
        ClientResponse<Student> clientResponse = clientRequest.get(Student.class);

        Student cStud = clientResponse.getEntity();
        Assert.assertTrue(cStud.getId() == 1);
    }

    @Test
    public void testGetStudents() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/report/students.xml");
        ClientResponse<List<Student>> clientResponse = clientRequest.get(new GenericType<List<Student>>() {
        });

        List<Student> cStud = clientResponse.getEntity();
        Assert.assertTrue(!cStud.isEmpty());
    }
}
