package com.ecwo.rest;

import com.ecwo.rest.model.Faculties;
import com.ecwo.rest.model.Specialities;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.GenericType;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Date: 01.02.16
 * Time: 22:48
 */
public class FacultyRestServiceTest {
    private static String srvPath = "http://192.168.56.1:8080/openAPI/rest";

    @Test
    public void testGetFaculties() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/faculties/.xml");
        ClientResponse<List<Faculties>> clientResponse = clientRequest.get(new GenericType<List<Faculties>>() {
        });
        Assert.assertTrue(clientResponse.getStatus() == 200);
        List<Faculties> cStud = clientResponse.getEntity();
        Assert.assertFalse(cStud.isEmpty());
        for (Faculties faculties : cStud) {
            System.out.printf("% 3d %s %s%n", faculties.getId(), faculties.getFacultyName(), faculties.getShortName());
        }

    }

    @Test
    public void testGetFacultyById() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/faculties/2.xml");
        ClientResponse<Faculties> clientResponse = clientRequest.get(Faculties.class);
        Faculties cStud = clientResponse.getEntity();
        Assert.assertTrue(cStud.getId() == 2);
    }

    @Test
    public void getSpecialities_fid2() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/faculties/2/specialities.xml");
        ClientResponse<List<Specialities>> clientResponse = clientRequest.get(new GenericType<List<Specialities>>() {
        });
        List<Specialities> cStud = clientResponse.getEntity();
        Assert.assertFalse(cStud.isEmpty());
    }

    @Test
    public void getSpecialities_All() throws Exception {
        ClientRequest clientRequest = new ClientRequest(srvPath + "/faculties/0/specialities.xml");
        ClientResponse<List<Specialities>> clientResponse = clientRequest.get(new GenericType<List<Specialities>>() {
        });
        Assert.assertTrue(clientResponse.getStatus()==200);
        List<Specialities> cStud = clientResponse.getEntity();
        Assert.assertFalse(cStud.isEmpty());
        Collections.sort(cStud,new Comparator<Specialities>() {
            @Override
            public int compare(Specialities o1, Specialities o2) {
                return o1.getSpecialityName().compareTo(o2.getSpecialityName());
            }
        });
        for (Specialities specialities : cStud) {
            System.out.printf("%4d %13s %s%n", specialities.getId(), specialities.getSpecialityCode().trim(), specialities.getSpecialityName());
        }

    }
}
