package com.ecwo.rest;

import com.ecwo.rest.model.SearchResult;
import com.ecwo.rest.model.Student;
import com.ecwo.rest.model.User;
import junit.framework.Assert;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientRequestFactory;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.resteasy.util.GenericType;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.HttpHeaders;
import java.net.URI;
import java.util.List;

/**
 * Date: 24.02.16
 * Time: 20:05
 */
public class AppServiceTest {
    private static ClientRequestFactory clientRequestFactory = null;

    @BeforeClass
    public static void setUp() throws Exception {
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());  //? https://habrahabr.ru/post/140181/
        clientRequestFactory = new ClientRequestFactory(URI.create(CommonRestServiceTest.srvPath));

    }

    @Test
    public void testFindStudentByLastNameAndPasspCode() throws Exception {

        ClientRequest clientRequest = clientRequestFactory.createRelativeRequest("/app/findStudentByLastNameAndPasspCode");
        final User user = new User();
        user.setEmail("Анденко");
        user.setPassword("187380");
        //---- UTF-8
//        clientRequest.accept("application/x-www-form-urlencoded; charset=UTF-8");//jxab error
        String contentType = clientRequest.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
        if (contentType == null || !contentType.contains("charset"))
            contentType = "application/x-www-form-urlencoded; charset=utf-8";
        clientRequest.header(HttpHeaders.CONTENT_TYPE, contentType);
        //----
        clientRequest.formParameter("email", user.getEmail());
        clientRequest.formParameter("password", user.getPassword());

     /*   ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(user);
        oos.flush();
        oos.close();
        MultivaluedMap<String,String> map = FormUrlEncodedProvider.parseForm(new ByteArrayInputStream(baos.toByteArray()));
        ((MultivaluedMapImpl) clientRequest.getFormParameters()).addAll((MultivaluedMapImpl) map);*/
        ClientResponse<Student> clientResponse = clientRequest.post(Student.class);

        if (clientResponse.getStatus() != 200)
            Assert.fail(clientResponse.getResponseStatus() + " " + clientRequest.getAttributes());

        Student userResp = clientResponse.getEntity();
        Assert.assertNotNull(userResp);
        Assert.assertEquals(user.getPassword(), userResp.getFistName());

    }

    @Test
    public void testSearch() throws Exception {
        ClientRequest clientRequest = clientRequestFactory.createRelativeRequest("/app/search");

        clientRequest.formParameter("proLevel", 0);
        clientRequest.formParameter("langA", 0);
        clientRequest.formParameter("langF", 0);
        clientRequest.formParameter("langG", 0);
        clientRequest.formParameter("aProgress", 0);
        clientRequest.formParameter("familyStatus", 0);
        clientRequest.formParameter("childs", 0);
        clientRequest.formParameter("cAge", 0);
        clientRequest.formParameter("carHave", 0);
        clientRequest.formParameter("driversL", 0);
        clientRequest.formParameter("intern", 0);
        clientRequest.formParameter("proSert", 0);
        clientRequest.formParameter("progWeb", 0);
        clientRequest.formParameter("gEditors", 0);
        clientRequest.formParameter("oEditors", 0);
        clientRequest.formParameter("e1c", 0);
        clientRequest.formParameter("eParus", 0);
        clientRequest.formParameter("conf", 0);
        clientRequest.formParameter("aPub", 0);
        clientRequest.formParameter("proAvard", 0);
        clientRequest.formParameter("commLvl", 0);
        clientRequest.formParameter("leadSc", 0);

        ClientResponse<SearchResult> clientResponse = clientRequest.post(new GenericType<List<SearchResult>>() {
        });
        Assert.assertTrue(clientResponse.getStatus() + " " + clientResponse.getHeaders() + " " + clientResponse.getAttributes(),
                clientResponse.getStatus() == 200);

        List<SearchResult> userResp = (List<SearchResult>) clientResponse.getEntity();
        Assert.assertNotNull(userResp);
    }
}
