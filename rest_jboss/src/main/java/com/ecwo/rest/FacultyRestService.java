package com.ecwo.rest;

import com.ecwo.rest.model.Faculties;
import com.ecwo.rest.model.Specialities;
import com.ecwo.rest.model.StudentGroupRest;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Date: 31.01.16
 * Time: 18:18
 */
@Path("/faculties")
public interface FacultyRestService {
    /**
     * http://dev:8080/openAPI/rest/faculties/.json
     * http://dev:8080/openAPI/rest/faculties/.xml
     * http://dev:8080/openAPI/rest/faculties/.xml?ayear=2016
     *
     * @param ayear Academic Year like 2015
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/")
    @Wrapped(element = "faculties")
    @GZIP
//    @Cache(maxAge = 3600)
    public List<Faculties> getFaculties(@DefaultValue("-1") @QueryParam("ayear") String ayear);

    /**
     * http://dev:8080/openAPI/rest/faculties/2.json
     * http://dev:8080/openAPI/rest/faculties/2.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{fid}")
    public Faculties getFacultyById(@PathParam("fid") Integer id);

    /**
     * http://dev:8080/openAPI/rest/faculties/6/specialities.json
     * http://dev:8080/openAPI/rest/faculties/6/specialities.xml
     * http://dev:8080/openAPI/rest/faculties/6/specialities.xml?ayear=2016
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{fid}/specialities")
    @GZIP
    public List<Specialities> getSpecialities(@PathParam("fid") Integer id, @DefaultValue("-1") @QueryParam("ayear") String ayear);

    /**
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2.json
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{fid}/specialities/{sid}")
    public Specialities getSpecialitiesById(@PathParam("fid") Integer fid, @PathParam("sid") Integer sid);

    /**
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2/sgroups.json
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2/sgroups.xml
     * список групп факультета/специальности, по умолчанию незакончившиеся на текущий учебный год
     *
     * @param course >0,  <5
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{fid}/specialities/{sid}/sgroups")
    @GZIP
    public List<StudentGroupRest> getStudentGroups(@PathParam("fid") Integer fid, @PathParam("sid") Integer sid,
                                                   @DefaultValue("0") @QueryParam("course") String course);
}
