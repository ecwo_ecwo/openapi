package com.ecwo.rest;

import com.ecwo.rest.model.Search;
import com.ecwo.rest.model.User;
import org.jboss.resteasy.annotations.Form;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Date: 02.02.16
 * Time: 14:03
 * http://www.slideshare.net/bjarlestam/introduction-to-jaxrs
 * http://www.slideshare.net/dmytro-chyzhykov/making-java-rest-with-jaxrs-20?related=1
 */
@Path("/app")
public interface AppService {
    @POST
    @Path("/findStudentByLastNameAndPasspCode")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED + "; charset=UTF-8")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findStudentByLastNameAndPasspCode(@Form User user);

    /**
     * see testForm.html
     * todo add X-Total-Count, page=0&per_page=100 ||offset=0&limit=100
     */
    @POST
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Wrapped(element = "rezults")
    public Response searchAPI(@Form Search s);

    /**
     * see testForm.html
     */
    @GET
    @Path("/searchp")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Wrapped(element = "rezults")
    public Response searchAPIPaged(@Form Search s,
                                   @DefaultValue("0") @QueryParam("offset") String offset,
                                   @DefaultValue("100") @QueryParam("limit") String limit);
}
