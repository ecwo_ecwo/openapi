package com.ecwo.rest;

import com.ecwo.rest.model.Student;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * User: dev
 * Date: 31.07.14
 * Time: 12:07
 * Reports
 */
public interface ReportRestService {

    /**
     * http://dev:8080/openAPI/rest/report/ver/
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ver")
    Response printVersion();

    /**
     * http://dev:8080/openAPI/rest/report/students/1.json
     * http://dev:8080/openAPI/rest/report/students/1.xml
     */

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/students/{id}")
    Student getStudentById(@PathParam("id") Integer id);

    /**
     * example List
     * http://dev:8080/openAPI/rest/report/students.json
     * http://dev:8080/openAPI/rest/report/students.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/students")
    @Wrapped(element = "students")
    @GZIP
    public List<Student> getStudents();

    /**
     * http://dev:8080/openAPI/rest/report/sg/{sgId}/formH503.json
     * http://dev:8080/openAPI/rest/report/sg/{sgId}/formH503.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/sg/{sgId}/formH503")
    public List<Student> getForm(@PathParam("sgId") Integer id);

}
