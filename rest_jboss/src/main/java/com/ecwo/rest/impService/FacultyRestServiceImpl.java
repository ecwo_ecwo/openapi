package com.ecwo.rest.impService;

import com.ecwo.project2002.server.exception.ApplicationServerSideException;
import com.ecwo.project2002.server.services.api.APISession;
import com.ecwo.project2002.server.services.api.APISessionHome;
import com.ecwo.rest.FacultyRestService;
import com.ecwo.rest.model.Faculties;
import com.ecwo.rest.model.Specialities;
import com.ecwo.rest.model.StudentGroupRest;
import org.jboss.resteasy.logging.Logger;

import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Date: 01.02.16
 * Time: 15:30
 */
//@EJBs(@EJB(name = "injected/ScheduleDataSourceSessionBean", mappedName = "ScheduleDataSourceSessionBean"))
public class FacultyRestServiceImpl implements FacultyRestService {
    private final Logger log = Logger.getLogger(FacultyRestServiceImpl.class);
    private static APISession localAPI;

    static {
        try {
            InitialContext ctx = new InitialContext();
            Object lookup2 = ctx.lookup(APISessionHome.class.getName());
            APISessionHome home2 = (APISessionHome) PortableRemoteObject.narrow(lookup2, APISessionHome.class);
            localAPI = home2.create();
        } catch (Exception ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * http://dev:8080/openAPI/rest/faculties/.json
     * http://dev:8080/openAPI/rest/faculties/.xml
     *
     * @param ayear Academic Year like 2015
     */
    @Override
    public List<Faculties> getFaculties(@DefaultValue("-1") @QueryParam("ayear") String ayear) {
        Integer acadYear = null;
        if (ayear.equalsIgnoreCase("-1")) {
            // FIXME CalendarUtil.AcademicYear...
            acadYear = 2016;

        } else acadYear = Integer.parseInt(ayear);
        try {

            List<String[]> facultiesDayDivision = localAPI.getFacultiesDayDivision(acadYear);
            List<Faculties> rez = new ArrayList<Faculties>(facultiesDayDivision.size());
            for (String[] filds : facultiesDayDivision) {
                rez.add(new Faculties(Integer.parseInt(filds[0]), filds[1], filds[2]));
            }

//            CacheControl cacheControl = new CacheControl();
//            cacheControl.setMaxAge((int) TimeUnit.MINUTES.toSeconds(2));
//            return Response.ok(rez).cacheControl(cacheControl).build();
            return rez;
        } catch (RemoteException e) {
            log.error(e.toString(), e);
        } catch (ApplicationServerSideException e) {
            log.error(e.toString(), e);
        }
        return Collections.emptyList();
    }

    /**
     * http://dev:8080/openAPI/rest/faculties/2.json
     * http://dev:8080/openAPI/rest/faculties/2.xml
     */
    @Override
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{fid}")
    public Faculties getFacultyById(@PathParam("fid") Integer id) {
        final List<Faculties> faculties1 = getFaculties("-1");
        for (Faculties faculties : faculties1) {
            if (faculties.getId().equals(id))
                return faculties;
        }

        return null;
    }

    /**
     * http://dev:8080/openAPI/rest/faculties/6/specialities.json
     * http://dev:8080/openAPI/rest/faculties/6/specialities.xml
     * http://dev:8080/openAPI/rest/faculties/6/specialities.xml?ayear=2016
     *  if fid==0 return all Specialities for fid in 1-6
     */
    @Override
    public List<Specialities> getSpecialities(@PathParam("fid") Integer id, @DefaultValue("-1") @QueryParam("ayear") String ayear) {
        Integer acadYear = Integer.parseInt(ayear);
        if (ayear.equalsIgnoreCase("-1")) {
            // FIXME CalendarUtil.AcademicYear...
            acadYear = 2016;

        } else acadYear = Integer.parseInt(ayear);
        List<Specialities> speciality = new ArrayList<Specialities>(20);
        try {
            final List<String[]> specialityStr = localAPI.getSpeciality(id);
            speciality = new ArrayList<Specialities>(speciality.size());
            for (String[] s : specialityStr) {
                speciality.add(new Specialities(
                        Integer.parseInt(s[0]), s[1], null, s[2]));
            }

        } catch (Exception e) {
            log.error(e.toString(), e);
        }

        return speciality;
    }

    /**
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2.json
     * http://dev:8080/openAPI/rest/faculties/6/specialities/2.xml
     */
    @Override
    public Specialities getSpecialitiesById(@PathParam("fid") Integer fid, @PathParam("sid") Integer sid) {
        final List<Specialities> specialities = getSpecialities(fid, "-1");
        for (Specialities speciality : specialities) {
            if (speciality.getId().equals(sid)) return speciality;
        }
        return null;
    }

    @Override
    public List<StudentGroupRest> getStudentGroups(@PathParam("fid") Integer fid, @PathParam("sid") Integer sid, @DefaultValue("0") @QueryParam("course") String course) {
        Integer cours = Integer.parseInt(course);
        try {
            List<StudentGroupRest> group = new ArrayList<StudentGroupRest>();
            final List<String[]> studentGroups = localAPI.getStudentGroups(fid, sid, cours);
            for (String[] str : studentGroups) {
                group.add(new StudentGroupRest(Integer.parseInt(str[0]), str[1], Integer.parseInt(str[2])));
            }
            return group;
        } catch (Exception e) {
            log.error(e.toString(), e);
            return Collections.emptyList();
        }
    }

}
