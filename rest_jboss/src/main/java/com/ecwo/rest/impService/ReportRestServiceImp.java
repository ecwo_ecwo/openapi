package com.ecwo.rest.impService;

import com.ecwo.rest.ReportRestService;
import com.ecwo.rest.model.Student;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * User: tor
 * Date: 31.07.14
 * Time: 18:36
 */
@Path("/report")
public class ReportRestServiceImp implements ReportRestService {
    public static final String version = "16.0314";//YY.MMdd

    /**
     * http://dev:8080/openAPI/rest/report/ver/
     *
     * @Produces("text/html")
     */
    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ver")
    public Response printVersion() {
        //
//        return Response.status(Response.Status.OK).entity(new JSONPObject("version","14.0731")).build();
        return Response.status(Response.Status.OK).entity("{\"version\":\"" + version + "\"}").build();
    }

    /**
     * http://dev:8080/openAPI/rest/report/students/1.json
     * http://dev:8080/openAPI/rest/report/students/1.xml
     */
    @Override
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/students/{id}")
    public Student getStudentById(@PathParam("id") Integer id) {
        return new Student(id, "Иванов", "Иван", "Иванович");
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/students")
    @Wrapped(element = "students")
    public List<Student> getStudents() {
        List<Student> list = new ArrayList<Student>(1);
        list.add(new Student(1, "Какая", "Вам", "Разница"));
        return list;
    }

    @Override
    public List<Student> getForm(@PathParam("sgId") Integer id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
