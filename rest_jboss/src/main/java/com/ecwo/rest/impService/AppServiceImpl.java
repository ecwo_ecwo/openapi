package com.ecwo.rest.impService;


import com.ecwo.project2002.server.exception.ApplicationServerSideException;
import com.ecwo.project2002.server.services.api.APISession;
import com.ecwo.project2002.server.services.api.APISessionHome;
import com.ecwo.rest.AppService;
import com.ecwo.rest.model.Search;
import com.ecwo.rest.model.SearchResult;
import com.ecwo.rest.model.Student;
import com.ecwo.rest.model.User;
import org.jboss.resteasy.annotations.Form;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 02.02.16
 * Time: 14:20
 * E-164 API Конкурс
 */
@Stateless
public class AppServiceImpl implements AppService {
    //    @EJB("ejb/APISession")
//    private static StudentSession studentSession;
    private static APISession localAPI;

    static {
        try {
            InitialContext ctx = new InitialContext();
            Object lookup2 = ctx.lookup(APISessionHome.class.getName());
            APISessionHome home2 = (APISessionHome) PortableRemoteObject.narrow(lookup2, APISessionHome.class);
            localAPI = home2.create();
        } catch (Exception ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    @POST
    @Path("/findStudentByLastNameAndPasspCode")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED+"; charset=UTF-8")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findStudentByLastNameAndPasspCode(@Form User user) {
        Response.ResponseBuilder builder;
        try {
            final String[] strings = localAPI.findByLastNameAndPassportCode(user.getEmail(), user.getPassword());
            if (strings == null) return Response.noContent().build();
            Student rez = new Student();
            rez.setId(Integer.valueOf(strings[0]));
            rez.setLastName(strings[1]);
            rez.setFistName(strings[2]);
            builder = Response.ok()/*.entity(vo)*/.entity(rez);
        } catch (RemoteException e) {
            builder = Response.serverError().entity(e.toString());
        } catch (ApplicationServerSideException e) {
            builder = Response.serverError().entity(e.toString());
        }
        return builder
//                .header("Access-Control-Allow-Origin", "*")
//                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .build();
    }

    @GET
    @Path("/searchp")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Wrapped(element = "rezults")
    public Response searchAPIPaged(@Form Search s,
                                   @DefaultValue("0") @QueryParam("offset") String offset,
                                   @DefaultValue("100") @QueryParam("limit") String limit) {
        Response.ResponseBuilder builder = null;
        List<SearchResult> rez = new ArrayList<SearchResult>();
        int count = 0;
        int offsetInt = Integer.parseInt(offset);
        int limitInt = Integer.parseInt(limit);
        try {
            List<String[]> list = localAPI.searchAPI(s.toStringArray());
            if (list != null) {
                count = list.size();
                for (int i = 0, listSize = list.size(); i < listSize; i++) {
                    String[] sr = list.get(i);
                    if (i >= offsetInt && i <= limitInt) rez.add(new SearchResult(sr));
                }
            } else return Response.noContent().build();
            count = rez.size();

            GenericEntity<List<SearchResult>> entity = new GenericEntity<List<SearchResult>>(rez) {
            };
            builder = Response.ok().entity(entity);
        } catch (ApplicationServerSideException e) {
            builder = Response.serverError().entity(e.toString());
        } catch (RemoteException e) {
            builder = Response.serverError().entity(e.toString());
        }
        return builder
                .header("X-Total-Count", String.valueOf(count))
                .build();
    }

    @POST
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Wrapped(element = "rezults")
    public Response searchAPI(@Form Search s) {
        Response.ResponseBuilder builder = null;
        List<SearchResult> rez = new ArrayList<SearchResult>();
        int count = 0;
        try {
            List<String[]> list = localAPI.searchAPI(s.toStringArray());
            if (list != null) {
                count = list.size();
                for (String[] sr : list) {
                    rez.add(new SearchResult(sr));
                }
            } else return Response.noContent().build();
            count = rez.size();
            GenericEntity<List<SearchResult>> entity = new GenericEntity<List<SearchResult>>(rez) {
            };
            builder = Response.ok().entity(entity);
        } catch (ApplicationServerSideException e) {
            builder = Response.serverError().entity(e.toString());
        } catch (RemoteException e) {
            builder = Response.serverError().entity(e.toString());
        }
        return builder
                .header("X-Total-Count", String.valueOf(count))
                .build();
    }
}
