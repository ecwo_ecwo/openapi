package com.ecwo.rest.impService;

import com.ecwo.rest.CommonRestService;
import com.ecwo.rest.model.PairsTime;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: tor
 * Date: 01.02.16
 * Time: 14:55
 */
@Path("/common")
public class CommonRestServiceImp implements CommonRestService {
    /**
     * http://dev:8080/openAPI/rest/common/pairs.json   mb bug
     * http://dev:8080/openAPI/rest/common/pairs..xml
     */
    @Override
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/pairs")
    @Wrapped(element = "pairs")
    public List<PairsTime> getPairs() {
        try {
            return Arrays.asList(
                    new PairsTime(0, "06:45", "07:30", "07:35", "08:20"),
                    new PairsTime(1, "08:30", "09:15", "09:20", "10:05"),
                    new PairsTime(2, "10:15", "11:00", "11:05", "11:50"),
                    new PairsTime(3, "12:10", "12:55", "13:00", "13:45"),
                    new PairsTime(4, "13:55", "14:40", "14:45", "15:30"),
                    new PairsTime(5, "15:50", "16:35", "16:40", "17:25"),
                    new PairsTime(6, "17:35", "18:20", "18:25", "19:10"),
                    new PairsTime(7, "19:20", "20:05", "20:10", "20:55"),
                    new PairsTime(8, "21:05", "21:50", "21:55", "22:40")
            );
        } catch (ParseException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * http://dev:8080/openAPI/rest/common/ayear.json
     * http://dev:8080/openAPI/rest/common/ayear.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/ayear")
    public Integer getAcademicYear() {
        // FIXME
        return 2016;
    }
}
