package com.ecwo.rest.app.filter;


import javax.servlet.*;
import java.io.IOException;

/**
 * Sets the UTF-8 encoding for all jsf pages
 * User: Boris
 * Date: 02.09.14
 * Time: 19:23
 */
public class Utf8EncodeFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
