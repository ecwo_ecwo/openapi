package com.ecwo.rest.app;

import com.ecwo.rest.impService.AppServiceImpl;
import com.ecwo.rest.impService.CommonRestServiceImp;
import com.ecwo.rest.impService.FacultyRestServiceImpl;
import com.ecwo.rest.impService.ReportRestServiceImp;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * User: tor
 * Date: 31.07.14
 * Time: 14:52
 * CacheControl -http://howtodoinjava.com/resteasy/jax-rs-resteasy-cache-control-with-etag-example/
 */
//@ApplicationPath("/rest")
public class OpenAPIApplication extends Application {
    private Set<Object> singletons = new HashSet<Object>();

    public OpenAPIApplication() {
//        singletons.add(new ContentTypeSetterPreProcessorInterceptor());
        singletons.add(new ReportRestServiceImp());
        singletons.add(new CommonRestServiceImp());
        singletons.add(new FacultyRestServiceImpl());
        singletons.add(new AppServiceImpl()); //4 Test
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
