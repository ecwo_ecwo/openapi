package com.ecwo.rest.app.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Date: 17.03.16
 * Time: 16:55
 */
public class CORSResponseFilter implements javax.servlet.Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
        //addHeader("access-control-allow-headers", "content-type, authorization");
        //addHeader("Access-Control-Allow-Origin", "http://concreteDomain.org"); //CORS 4 concreteDomain.org
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
