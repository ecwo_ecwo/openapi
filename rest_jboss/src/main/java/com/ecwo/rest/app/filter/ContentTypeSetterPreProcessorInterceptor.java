package com.ecwo.rest.app.filter;

import org.jboss.resteasy.annotations.interception.ClientInterceptor;
import org.jboss.resteasy.annotations.interception.HeaderDecoratorPrecedence;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.spi.interception.ClientExecutionContext;
import org.jboss.resteasy.spi.interception.ClientExecutionInterceptor;

import javax.ws.rs.core.HttpHeaders;

/**
 * Date: 14.03.16
 * Time: 2:38
 */


//@Provider
//@ServerInterceptor
//public class ContentTypeSetterPreProcessorInterceptor implements PreProcessInterceptor {
//
//    public ServerResponse preProcess(HttpRequest request, ResourceMethod method) throws Failure, WebApplicationException {
//        request.setAttribute(InputPart.DEFAULT_CONTENT_TYPE_PROPERTY, "*/*; charset=UTF-8");
//        return null;
//    }
//
//}

@ClientInterceptor
@HeaderDecoratorPrecedence
public class ContentTypeSetterPreProcessorInterceptor implements ClientExecutionInterceptor {
    public static final String FORM_CONTENT_TYPE = "application/x-www-form-urlencoded";
    public static final String FORM_CONTENT_TYPE_WITH_CHARSET = "application/x-www-form-urlencoded; charset=utf-8";

    @Override
    public ClientResponse execute(ClientExecutionContext context) throws Exception {
        String contentType = context.getRequest().getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
        if (formWithoutCharset(contentType)) {
            context.getRequest().header(HttpHeaders.CONTENT_TYPE, FORM_CONTENT_TYPE_WITH_CHARSET);
        }
        return context.proceed();
    }

    private boolean formWithoutCharset(String contentType) {
        return contentType != null
                && contentType.contains(FORM_CONTENT_TYPE)
                && !contentType.contains("charset");
    }
}
