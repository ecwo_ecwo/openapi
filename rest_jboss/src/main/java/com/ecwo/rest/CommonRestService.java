package com.ecwo.rest;

import com.ecwo.rest.model.PairsTime;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Date: 31.01.16
 * Time: 18:16
 */
public interface CommonRestService {
    /**
     * http://dev:8080/openAPI/rest/common/pairs.json
     * http://dev:8080/openAPI/rest/common/pairs.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/pairs")
    @Wrapped(element = "pairs")
    List<PairsTime> getPairs();

    /**
     * http://dev:8080/openAPI/rest/common/ayear.json
     * http://dev:8080/openAPI/rest/common/ayear.xml
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/ayear")
    Integer getAcademicYear();
}
