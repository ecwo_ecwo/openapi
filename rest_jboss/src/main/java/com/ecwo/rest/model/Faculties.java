package com.ecwo.rest.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 01.02.16
 * Time: 15:24
 */
@XmlRootElement(name = "faculties")
public class Faculties {
    private Integer id;
    private String facultyName;
    private String shortName;

    public Faculties() {
    }

    public Faculties(Integer id, String facultyName, String shortName) {
        this.id = id;
        this.facultyName = facultyName;
        this.shortName = shortName;
    }

    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElement
    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @XmlElement
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
