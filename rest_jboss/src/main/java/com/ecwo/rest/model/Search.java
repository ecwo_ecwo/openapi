package com.ecwo.rest.model;

import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 02.02.16
 * Time: 19:05
 */
@XmlRootElement(name = "search")
public class Search {
    @FormParam("proLevel")
    private String proLevel = "0";

    @FormParam("spclty")
    private String speciality = "0";

    @FormParam("langA")
    private String langA = "0";
    @FormParam("langF")
    private String langF = "0";

    @FormParam("langG")
    private String langG = "0";

    @FormParam("aProgress")
    private String aProgress = "0";

    @FormParam("familyStatus")
    private String familyStatus = "0";


    @FormParam("childs")
    private String childs = "0";

    @FormParam("cAge")
    private String childrenAge = "0";


    @FormParam("carHave")
    private String carHave = "0";


    @FormParam("driversL")
    private String driversL = "0";


    @FormParam("intern")
    private String internships = "0";


    @FormParam("proSert")
    private String proSert = "0";


    @FormParam("progWeb")
    private String progWeb = "0";


    @FormParam("gEditors")
    private String gEditors = "0";


    @FormParam("oEditors")
    private String oEditors = "0";


    @FormParam("e1c")
    private String e1c = "0";


    @FormParam("eParus")
    private String eParus = "0";


    @FormParam("conf")
    private String conf = "0";


    @FormParam("aPub")
    private String aPub = "0";


    @FormParam("proAvard")
    private String proAvard = "0";


    @FormParam("commLvl")
    private String commLvl = "0";


    @FormParam("leadSc")
    private String leadSc = "0";

    public Search() {
    }

    @XmlElement
    public String getLangA() {
        return langA;
    }

    public void setLangA(String langA) {
        this.langA = langA;
    }

    @XmlElement
    public String getLangF() {
        return langF;
    }

    public void setLangF(String langF) {
        this.langF = langF;
    }

    @XmlElement
    public String getLangG() {
        return langG;
    }

    public void setLangG(String langG) {
        this.langG = langG;
    }

    @XmlElement
    public String getaProgress() {
        return aProgress;
    }

    public void setaProgress(String aProgress) {
        this.aProgress = aProgress;
    }

    @XmlElement
    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    @XmlElement
    public String getProLevel() {
        return proLevel;
    }

    public void setProLevel(String proLevel) {
        this.proLevel = proLevel;
    }

    @XmlElement
    public String getChilds() {
        return childs;
    }

    public void setChilds(String childs) {
        this.childs = childs;
    }

    @XmlElement
    public String getChildrenAge() {
        return childrenAge;
    }

    public void setChildrenAge(String childrenAge) {
        this.childrenAge = childrenAge;
    }

    @XmlElement
    public String getCarHave() {
        return carHave;
    }

    public void setCarHave(String carHave) {
        this.carHave = carHave;
    }

    @XmlElement
    public String getDriversL() {
        return driversL;
    }

    public void setDriversL(String driversL) {
        this.driversL = driversL;
    }

    @XmlElement
    public String getInternships() {
        return internships;
    }

    public void setInternships(String internships) {
        this.internships = internships;
    }

    @XmlElement
    public String getProSert() {
        return proSert;
    }

    public void setProSert(String proSert) {
        this.proSert = proSert;
    }

    @XmlElement
    public String getProgWeb() {
        return progWeb;
    }

    public void setProgWeb(String progWeb) {
        this.progWeb = progWeb;
    }

    @XmlElement
    public String getgEditors() {
        return gEditors;
    }

    public void setgEditors(String gEditors) {
        this.gEditors = gEditors;
    }

    @XmlElement
    public String getoEditors() {
        return oEditors;
    }

    public void setoEditors(String oEditors) {
        this.oEditors = oEditors;
    }

    @XmlElement
    public String getE1c() {
        return e1c;
    }

    public void setE1c(String e1c) {
        this.e1c = e1c;
    }

    @XmlElement
    public String geteParus() {
        return eParus;
    }

    public void seteParus(String eParus) {
        this.eParus = eParus;
    }

    @XmlElement
    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    @XmlElement
    public String getaPub() {
        return aPub;
    }

    public void setaPub(String aPub) {
        this.aPub = aPub;
    }

    @XmlElement
    public String getProAvard() {
        return proAvard;
    }

    public void setProAvard(String proAvard) {
        this.proAvard = proAvard;
    }

    @XmlElement
    public String getCommLvl() {
        return commLvl;
    }

    public void setCommLvl(String commLvl) {
        this.commLvl = commLvl;
    }

    @XmlElement
    public String getLeadSc() {
        return leadSc;
    }

    public void setLeadSc(String leadSc) {
        this.leadSc = leadSc;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("proLevel='").append(proLevel).append('\'');
        sb.append(", langA='").append(langA).append('\'');
        sb.append(", langF='").append(langF).append('\'');
        sb.append(", langG='").append(langG).append('\'');
        sb.append(", aProgress='").append(aProgress).append('\'');
        sb.append(", familyStatus='").append(familyStatus).append('\'');
        sb.append(", childs='").append(childs).append('\'');
        sb.append(", childrenAge='").append(childrenAge).append('\'');
        sb.append(", carHave='").append(carHave).append('\'');
        sb.append(", driversL='").append(driversL).append('\'');
        sb.append(", internships='").append(internships).append('\'');
        sb.append(", proSert='").append(proSert).append('\'');
        sb.append(", progWeb='").append(progWeb).append('\'');
        sb.append(", gEditors='").append(gEditors).append('\'');
        sb.append(", oEditors='").append(oEditors).append('\'');
        sb.append(", e1c='").append(e1c).append('\'');
        sb.append(", eParus='").append(eParus).append('\'');
        sb.append(", conf='").append(conf).append('\'');
        sb.append(", aPub='").append(aPub).append('\'');
        sb.append(", proAvard='").append(proAvard).append('\'');
        sb.append(", commLvl='").append(commLvl).append('\'');
        sb.append(", leadSc='").append(leadSc).append('\'');
        sb.append(", spclty='").append(speciality).append('\'');
        return sb.toString();
    }

    public String[] toStringArray() {
        final String[] rez = {
                proLevel,
                langA,
                langF,
                langG,
                aProgress,
                familyStatus,
                childs,
                childrenAge,
                carHave,
                driversL,
                internships,
                proSert,
                progWeb,
                gEditors,
                oEditors,
                e1c,
                eParus,
                conf,
                aPub,
                proAvard,
                commLvl,
                leadSc,
                speciality};
        for (int i = 0, rezLength = rez.length; i < rezLength; i++) {
            String s = rez[i];
            if (s == null) rez[i] = "0";
        }

        return rez;
    }

    public void fill(String[] s) {
        this.proLevel = s[0];
        this.langA = s[1];
        this.langF = s[2];
        this.langG = s[3];
        this.aProgress = s[4];
        this.familyStatus = s[5];
        this.childs = s[6];
        this.childrenAge = s[7];
        this.carHave = s[8];
        this.driversL = s[9];
        this.internships = s[10];
        this.proSert = s[11];
        this.progWeb = s[12];
        this.gEditors = s[13];
        this.oEditors = s[14];
        this.e1c = s[15];
        this.eParus = s[16];
        this.conf = s[17];
        this.aPub = s[18];
        this.proAvard = s[19];
        this.commLvl = s[20];
        this.leadSc = s[21];
        this.speciality = s[22];
    }

    @XmlElement
    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
