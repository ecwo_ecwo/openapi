package com.ecwo.rest.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
* User: tor
* Date: 31.07.14
* Time: 18:58
* http://stackoverflow.com/questions/17614784/resteasy-service-for-producing-xml-and-returning-array-list-of-jaxb-objects-givi
*/
@XmlRootElement(name = "student")
//@XmlAccessorType(XmlAccessType.FIELD)
public class Student {
    public Student() {
    }

    public Student(Integer id, String lastName, String fistName, String middleName) {
        this.id = id;
        this.lastName = lastName;
        this.fistName = fistName;
        this.middleName = middleName;
    }

    private Integer id;
    private String lastName;
    private String fistName;
    private String middleName;
    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @XmlElement
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @XmlElement
    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    @XmlElement
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
