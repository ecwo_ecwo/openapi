package com.ecwo.rest.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 02.02.16
 * Time: 15:47
 * for -list
 */
@XmlRootElement(name = "Specialities")
public class Specialities {
    private Integer id;
    private String specialityCode;
    private String specialityName;
    private String specialityNumber;
    private String shortName;
    private String qualName; // точное название квалификации

    public Specialities() {
    }

    public Specialities(Integer id, String specialityName, String specialityNumber, String specialityCode) {
        this.id = id;
        this.specialityName = specialityName;
        this.specialityNumber = specialityNumber;
        this.specialityCode = specialityCode;
        this.shortName = "";
        this.qualName = "";

    }

    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElement
    public String getSpecialityCode() {
        return specialityCode;
    }

    public void setSpecialityCode(String specialityCode) {
        this.specialityCode = specialityCode;
    }

    @XmlElement
    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    @XmlElement
    public String getSpecialityNumber() {
        return specialityNumber;
    }

    public void setSpecialityNumber(String specialityNumber) {
        this.specialityNumber = specialityNumber;
    }

    @XmlElement
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getQualName() {
        return qualName;
    }

    @XmlElement
    public void setQualName(String qualName) {
        this.qualName = qualName;
    }
}
