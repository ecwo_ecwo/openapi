package com.ecwo.rest.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 17.02.16
 * Time: 17:20
 */
@XmlRootElement(name = "StudentGroup")
public class StudentGroupRest {
    private Integer id;
    private String name;
    private Integer course;

    public StudentGroupRest() {
    }

    public StudentGroupRest(Integer id, String name, Integer course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }

    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }
}
