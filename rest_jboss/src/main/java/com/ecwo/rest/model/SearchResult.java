package com.ecwo.rest.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;

/**
 * Date: 26.02.16
 * Time: 15:52
 */
@XmlRootElement(name = "rezults")
public class SearchResult {

    private String studentId;

    private Search rez;

    public SearchResult() {
    }

    public SearchResult(String studentId, Search rez) {
        this.studentId = studentId;
        this.rez = rez;
    }

    public SearchResult(String[] sr) {
        System.out.println("sr = " + Arrays.toString(sr));
        studentId = sr[0];
        rez = new Search();
        rez.fill(Arrays.copyOfRange(sr, 1, sr.length));
    }

    @XmlAttribute
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @XmlElement(name = "search", required = true)
    public Search getRez() {
        return rez;
    }

    public void setRez(Search rez) {
        this.rez = rez;
    }
}
