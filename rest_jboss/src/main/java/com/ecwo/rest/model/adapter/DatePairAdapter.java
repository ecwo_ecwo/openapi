package com.ecwo.rest.model.adapter;

import com.ecwo.rest.model.PairsTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date: 01.02.16
 * Time: 16:07
 */
public class DatePairAdapter extends XmlAdapter<String, Date> {

    private SimpleDateFormat dateFormat = PairsTime.timeFormat;// new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public String marshal(Date v) throws Exception {
        return dateFormat.format(v);
    }

    @Override
    public Date unmarshal(String v) throws Exception {
        return dateFormat.parse(v);
    }

}