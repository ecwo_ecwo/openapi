package com.ecwo.rest.model;

import com.ecwo.rest.model.adapter.DatePairAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Tor
 * Date: 01.02.16
 * Time: 14:43
 */
@XmlRootElement(name = "pair")
public class PairsTime {
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private Integer num;
    private Date start, breakStart, breakEnd, end;

    public PairsTime() {
    }

    public PairsTime(Integer num, Date start, Date breakStart, Date breakEnd, Date end) {
        this.num = num;
        this.start = start;
        this.breakStart = breakStart;
        this.breakEnd = breakEnd;
        this.end = end;
    }

    public PairsTime(Integer num, String start, String breakStart, String breakEnd, String end) throws ParseException {
        this.num = num;
        this.start = toDate(start);
        this.breakStart = toDate(breakStart);
        this.breakEnd = toDate(breakEnd);
        this.end = toDate(end);
    }

    private Date toDate(String s) throws ParseException {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(timeFormat.parse(s));
        return c.getTime();
    }

    @XmlAttribute
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DatePairAdapter.class)
    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DatePairAdapter.class)
    public Date getBreakStart() {
        return breakStart;
    }

    public void setBreakStart(Date breakStart) {
        this.breakStart = breakStart;
    }

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DatePairAdapter.class)
    public Date getBreakEnd() {
        return breakEnd;
    }

    public void setBreakEnd(Date breakEnd) {
        this.breakEnd = breakEnd;
    }

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DatePairAdapter.class)
    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
