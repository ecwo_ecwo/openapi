package com.ecwo.rest.model;

import javax.ws.rs.FormParam;
import java.io.Serializable;

/**
 * Date: 02.02.16
 * Time: 13:52
 * for test
 */
public class User implements Serializable {
    @FormParam("email")
    private String email;
    @FormParam("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
